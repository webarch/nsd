# Webarchitects Name Server Daemon (NSD) Ansible role

The [NLnet Labs Name Server Daemon (NSD)](https://www.nlnetlabs.nl/projects/nsd/about/) is an authoritative DNS name server.

## Notes

The NSD configuration files look like YAML but booleans must not be quoted and must be `yes` or `no`, not `true` or `false` or `"yes"` or `"no"` as a result [ansible.builtin.to_nice_yaml](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/to_nice_yaml_filter.html) can't be used for templating the configuration files, if the NSD config is represented as YAML.

The [template this role uses](templates/nsd.conf.j2) is designed to accept YAML for the NSD config and convert it into valid NSD config, so, for example this YAML:

```yaml
nsd_conf:
  - path: /etc/nsd/nsd.conf
    state: present
    conf:
      server:
        log-only-syslog: true
        ip-address: "{{ ansible_facts.default_ipv4.address }}"
      include: "/etc/nsd/nsd.conf.d/*.conf"
```

Produces a `/etc/nsd/nsd.conf` file containing:

```yaml
 server:
        log-only-syslog: yes
        ip-address: 192.168.0.1
 include: /etc/nsd/nsd.conf.d/*.conf
```

A key effect of this role using valid YAML for the NSD config is that each zone file needs to have a seperate NSD config file since keys can't be duplicated, this can't be done:

```yaml
nsd_conf:
  - path: /etc/nsd/nsd.conf.d/zones.conf
    state: present
    conf:
      zone:
        name: example.com
        zonefile: /etc/nsd/zones/example.com.host
      zone:
        name: example.org
        zonefile: /etc/nsd/zones/example.org.host
```

A NSD conf file is needed per zonefile:

```yaml
nsd_conf:
  - path: /etc/nsd/nsd.conf.d/example.com.conf
    state: present
    conf:
      zone:
        name: example.com
        zonefile: /etc/nsd/zones/example.com.host
  - path: /etc/nsd/nsd.conf.d/example.org.conf
    state: present
    conf:
      zone:
        name: example.org
        zonefile: /etc/nsd/zones/example.org.host
```

## TODO

1. Add a check that all `/etc/nsd/nsd.conf.d/*.conf` files are set to have a `state` of `absent` or `present` and that there are no files present that this role doesn't manage.
